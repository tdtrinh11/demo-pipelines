from pathlib import Path

import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.impute import KNNImputer, SimpleImputer
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder, RobustScaler

titanic_path = (
    "https://media.githubusercontent.com/media/tiepvupsu/tabml_data/master/titanic/"
)
df_train_full = pd.read_csv(titanic_path + "train.csv")
df_test = pd.read_csv(titanic_path + "test.csv")

df_train_full.drop(columns=["Cabin"])
df_test.drop(columns=["Cabin"])

df_train, df_val = train_test_split(df_train_full, test_size=0.1)
X_train = df_train.copy()
y_train = X_train.pop("Survived")

X_val = df_val.copy()
y_val = X_val.pop("Survived")

cat_cols = ["Embarked", "Sex", "Pclass"]
cat_transformer = Pipeline(
    steps=[
        ("imputer", SimpleImputer(strategy="most_frequent")),
        ("onehot", OneHotEncoder(handle_unknown="ignore", sparse=False)),
    ]
)

num_cols = ["Age", "Fare"]
num_transformer = Pipeline(
    steps=[("imputer", KNNImputer(n_neighbors=5)), ("scaler", RobustScaler())]
)

preprocessor = ColumnTransformer(
    transformers=[
        ("num", num_transformer, num_cols),
        ("cat", cat_transformer, cat_cols),
    ]
)

# Full training pipeline
full_pp = Pipeline(
    steps=[("preprocessor", preprocessor), ("classifier", RandomForestClassifier())]
)

# training
full_pp.fit(X_train, y_train)

# training metric
y_train_pred = full_pp.predict(X_train)
print(
    f"Accuracy score on train data: {accuracy_score(list(y_train), list(y_train_pred)):.2f}"
)

# validation metric
y_pred = full_pp.predict(X_val)
print(
    f"Accuracy score on validation data: {accuracy_score(list(y_val), list(y_pred)):.2f}"
)

